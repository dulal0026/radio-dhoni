//
//  RDImageSlider.m
//  Radio Dhoni
//
//  Created by MAC on 10/8/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import "RDImageSlider.h"
#import "SwipeView.h"
#import "SMPageControl.h"


#define ANIMATE_DURATION 1.0

@interface RDImageSlider ()<SwipeViewDelegate, SwipeViewDataSource>

@property (weak, nonatomic) IBOutlet SwipeView *swipeView;
///@property (nonatomic)BOOL rightFlag;
@property (weak, nonatomic) IBOutlet SMPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;


@property (strong, nonatomic) NSTimer *slideTimer;

@end

@implementation RDImageSlider

-(void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    
    self.pageControl.pageIndicatorImage = [UIImage imageNamed:@"slide_dott"];
    self.pageControl.currentPageIndicatorImage = [UIImage imageNamed:@"slide_dot"];
    self.pageControl.backgroundColor = [UIColor clearColor];
    self.pageControl.userInteractionEnabled = NO;
    
    self.swipeView.wrapEnabled = YES;
}

-(void)didMoveToSuperview {
    [super didMoveToSuperview];
    // Prevent some strange reordering in runtime
    [self sendSubviewToBack:self.swipeView];
}
- (void)dealloc
{
    [self.timer invalidate];
}
#pragma mark - Public Methods

+(instancetype)getFromNib {
    NSString *nibName = NSStringFromClass(self);
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id currrentObject in nibObjects) {
        if ([currrentObject isKindOfClass:self]) {
            return currrentObject;
        }
    }
    return nil;
}

#pragma mark - Public Methods

+(RDImageSlider *)addInstanceInContainer:(UIView *)container {
    RDImageSlider *slider = [self getFromNib];
    if (slider) {
        [container addSubview:slider];

        
        slider.swipeView.alignment = SwipeViewAlignmentCenter;
        
        [slider setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [container addConstraints:[NSLayoutConstraint
                                   constraintsWithVisualFormat:@"H:|-0-[slider]-0-|"
                                   options:NSLayoutFormatDirectionLeadingToTrailing
                                   metrics:nil
                                   views:NSDictionaryOfVariableBindings(slider)]];
        [container addConstraints:[NSLayoutConstraint
                                   constraintsWithVisualFormat:@"V:|-0-[slider]-0-|"
                                   options:NSLayoutFormatDirectionLeadingToTrailing
                                   metrics:nil
                                   views:NSDictionaryOfVariableBindings(slider)]];
    }
    return slider;
}

-(void)setImages:(NSArray *)images{
    _images = images;
  
    if (self.slideTimer) {
        [self.slideTimer invalidate];
    }
    
    if ([images count] > 1) {
        self.slideTimer = [NSTimer scheduledTimerWithTimeInterval:4.0
                                                           target:self
                                                         selector:@selector(animateNewImage:)
                                                         userInfo:nil
                                                          repeats:YES];
    } else {
        [self.slideTimer invalidate];
        self.slideTimer = nil;
    }
    

    [self.swipeView reloadData];
}
-(void)animateNewImage:(NSTimer*)timer {
    if (self.swipeView.dragging) {
        return;
    }
    
    [self showNextImage:nil];
}
#pragma mark - Getter/Setter

-(NSInteger)currentPage {
    return self.swipeView.currentPage;
}

#pragma mark - User Actions

- (IBAction)showPreviousImage:(id)sender {
    [self.swipeView scrollByNumberOfItems:-1 duration:ANIMATE_DURATION];
}

- (IBAction)showNextImage:(id)sender {
    [self.swipeView scrollByNumberOfItems:1 duration:ANIMATE_DURATION];
}

#pragma mark mark - SwipeView Delegate/DataSource methods

-(NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView {
    NSInteger numberOfImages = self.images? [self.images count] : 0;
    self.pageControl.numberOfPages = numberOfImages;
    self.pageControl.currentPage = 0;
    return numberOfImages;
}

-(CGSize)swipeViewItemSize:(SwipeView *)swipeView {
    return self.frame.size;
}

-(UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    UIImageView *imgView = nil;
     [imgView setContentMode:UIViewContentModeScaleToFill];
    if (view && [view isKindOfClass:[UIImageView class]]) {
        NSLog(@"Prevent showing reused image for a while");
        imgView = (UIImageView *) view;
        // Prevent showing reused image for a while
        [imgView setImage:nil];
        
    } else {
        NSLog(@"CGRectZero");
        // Frame should be recalculated by SwipeView
        imgView = [[UIImageView alloc] initWithFrame:CGRectZero];
       
        [imgView setClipsToBounds:YES];
    }
    NSString *imgURL =[NSString stringWithFormat:@"%@",self.images[index]];
    NSString* encodedImageUrl = [imgURL stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];
  
    //[imgView setImageWithURL:[NSURL URLWithString:imgURL]
            //placeholderImage:[UIImage imageNamed:@"placeHolder"]];
    [imgView sd_setImageWithURL:[NSURL URLWithString:encodedImageUrl]
                      placeholderImage:[UIImage imageNamed:@"Logo"]];
    return imgView;
}
-(void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView {
    self.pageControl.currentPage = swipeView.currentPage;
}
-(void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index {
    NSLog(@"tap on item no:%ld",(long)index);
    if ([self.delegate respondsToSelector:@selector(eTBImageSlider:didSelectIndex:)]) {
        [self.delegate eTBImageSlider:self didSelectIndex:index];
    }
}
@end
