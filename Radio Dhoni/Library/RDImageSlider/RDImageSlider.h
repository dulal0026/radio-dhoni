//
//  RDImageSlider.h
//  Radio Dhoni
//
//  Created by MAC on 10/8/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RDImageSliderDelegate;
@interface RDImageSlider : UIView

@property(nonatomic,strong)NSTimer *timer;
@property (copy, nonatomic) NSArray *images;

@property (readonly) NSInteger currentPage;
@property (nonatomic, weak) id <RDImageSliderDelegate> delegate;

+(RDImageSlider *) addInstanceInContainer:(UIView *) container;
-(void) setImages:(NSArray *)images;

@end

@protocol RDImageSliderDelegate <NSObject>

@optional
- (void)eTBImageSlider:(RDImageSlider *)eTBImageSlider didSelectIndex:(NSInteger)index;
@end