//
//  RDARequestManager.m
//  Radio Dhoni
//
//  Created by MAC on 10/11/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import "RDRequestManager.h"

@implementation RDRequestManager

+(RDRequestManager *) manager {
    static dispatch_once_t pred;
    static RDRequestManager *shared = nil;
    dispatch_once(&pred, ^{
        NSURL *baseURL = [NSURL URLWithString:@"http://radiodhoni.sslwireless.com/appapi/"];
        
        
        shared = [[RDRequestManager alloc] initWithBaseURL:baseURL];
        
        AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer new];
        
        serializer.readingOptions=NSJSONReadingAllowFragments;
        serializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
        shared.responseSerializer = serializer;
        
        shared.requestSerializer = [AFJSONRequestSerializer serializer];
        
    });
    return shared;
}
@end
