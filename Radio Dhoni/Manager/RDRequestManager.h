//
//  RDARequestManager.h
//  Radio Dhoni
//
//  Created by MAC on 10/11/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

@interface RDRequestManager : AFHTTPRequestOperationManager
+(RDRequestManager *) manager;
@end
