//
//  Alert Manager
//
//  Created by Dulal Hossain on 03/11/2014.
//  Copyright (c) 2013 Aviel Gross. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface AlertManager : NSObject
{
    UIAlertView *alert;
}
+(void)showAlertSingle:(NSString *)title msg:(NSString *)msg;
-(void)showConfirmationAlert:(NSString *)title msg:(NSString *)msg;

@end
