//
//  Alert Manager
//
//  Created by Dulal Hossain on 03/11/2014.
//  Copyright (c) 2013 Aviel Gross. All rights reserved.
//

#import "AlertManager.h"

@implementation AlertManager

+(void)showAlertSingle:(NSString *)title msg:(NSString *)msg
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(msg, msg) delegate:self cancelButtonTitle:NSLocalizedString(@"CLOSE", @"CLOSE") otherButtonTitles:nil, nil];
	[alert show];
}
-(void)showConfirmationAlert:(NSString *)title msg:(NSString *)msg{
    alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(title, title) message:Nil delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil, nil];
    [alert show];
  
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        
    }
}

@end
