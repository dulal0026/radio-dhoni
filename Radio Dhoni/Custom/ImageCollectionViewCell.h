//
//  ImageCollectionViewCell.h
//  PremedMe
//
//  Created by ASHIKUR RAHMAN ROBIN on 9/8/15.
//  Copyright (c) 2015 ASHIKUR RAHMAN ROBIN. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "PreviewableImageView.h"

@interface ImageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
