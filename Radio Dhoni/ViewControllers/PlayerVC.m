//
//  PlayerVC.m
//  Radio Dhoni
//
//  Created by MAC on 10/8/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import "PlayerVC.h"
#import "UIViewController+MMDrawerController.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Social/Social.h>

@interface PlayerVC ()

@property (weak, nonatomic) IBOutlet UIButton *minusButton;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;

@end

@implementation PlayerVC{
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Radio Dhoni";
    self.messageTxtView.delegate=self;
    
    self.player = [[AVPlayer alloc] init];
   // self.player = [self.player initWithURL:[NSURL URLWithString:kRadio_Today_BaseURL]];
    self.player = [self.player initWithURL:[NSURL URLWithString:kRadio_Dhoni_BaseURL]];
    [self.player play];
    
    [self.playButton setImage:[UIImage imageNamed:@"Stop"] forState:UIControlStateNormal ];
    self.isPlay = true;
    
    self.imageSlider = [RDImageSlider addInstanceInContainer:self.imageSliderContainer];
    self.imageSlider.delegate=self;
    [self getSliderImages];
    [self goOnline];
    [self TotalUserOnlineApi];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
//------------------------------------------------------
#pragma mark - IBActions
//------------------------------------------------------
- (IBAction)showMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:NULL];
}
- (IBAction)playPauseButtonAction:(id)sender {
    if (self.isPlay) [self.player pause];
    else [self.player play];
    [self.playButton setImage:[UIImage imageNamed:self.isPlay?@"Play":@"Stop"] forState:UIControlStateNormal];
    self.isPlay = !self.isPlay;
}

- (IBAction)powerButtonAction:(id)sender {
    [self.playButton setImage:[UIImage imageNamed:@"Play"] forState:UIControlStateNormal];
    [self.player pause];
    self.isPlay = false;
}

-(IBAction)sliderValueChanged:(UIButton *)sender
{
    float cVol = self.player.volume;
    cVol += (sender == self.plusButton)? 0.1 : -0.1;
    cVol = MAX(0, MIN(1, cVol));
    self.player.volume = cVol;
}


- (IBAction)facebookShareButtonAction:(id)sender
{
    NSLog(@"facebookShareButtonAction");
    /*
     NSURL *imageURL = nil;
     MCATModel *mcat = [PremedMeSessionManager manager].mcatInfo;
     if (mcat && mcat.picture.count) {
     imageURL = ((PictureModel *)mcat.picture[0]).url;
     } else {
     [SVProgressHUD showErrorWithStatus:@"Latest image is not available to share!"];
     return;
     }
     */
    FBSDKShareLinkContent *content = [FBSDKShareLinkContent new];
    content.contentTitle = @"Radio Today";
    //content.imageURL = imageURL;
    //content.contentURL = [NSURL URLWithString:@"http://premedconnection.com/"];
    //content.contentDescription = self.desTextView.text;
    
    [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil];
}
-(IBAction)twitterButtonAction:(id)sender{
    NSLog(@"Twitter Share Button Action");
    //
    //    UIImage *image = nil;
    //    ImageCollectionViewCell *imageCell = (ImageCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    //    if (imageCell && imageCell.imageView.image) {
    //        image = imageCell.imageView.image;
    //    } else {
    //        [SVProgressHUD showErrorWithStatus:@"Latest image is not available or not loaded!"];
    //        return;
    //    }
    
    SLComposeViewController *social = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    //[social addImage:image];
    [social setInitialText:@"Twitte share via radio today"];
    
    [self presentViewController:social animated:YES completion:nil];
}
- (IBAction) googleShareButtonAction: (id)sender {
    id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
    [shareBuilder setPrefillText:@"Share via radio dhoni"];
   // [shareBuilder setURLToShare:[NSURL URLWithString:@"https://www.example.com/restaurant/sf/1234567/"]];
    [shareBuilder open];
}
- (IBAction) sendButtonAction: (id)sender{
    NSString *message = self.messageTxtView.text;

    if([message isEqualToString:placeHolderMessage] || message.length==0){
        [AlertManager showAlertSingle:@"Warning" msg:@"Please enter a comment."];
        return;
    }
    [self.messageTxtView resignFirstResponder];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
  
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:message forKey:@"comment"];
    
    RDRequestManager *manager = [RDRequestManager manager];
    [manager GET:@"index.php" parameters:params
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
              NSDictionary *jsonDict = (NSDictionary *) responseObject;
              if([[jsonDict objectForKey:@"response"] isEqualToString:@"success"])
              {
                  //                  self.lblNowOnline.text = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"count"]];
                  self.messageTxtView.text=placeHolderMessage;
                  [SVProgressHUD showSuccessWithStatus:@"Sending message successfully!"];
              }
              else
                  [SVProgressHUD showErrorWithStatus:@"An error occured during sending your message!"];
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              [SVProgressHUD showErrorWithStatus:@"An error occured during sending your message!"];
          }
     ];
}
- (IBAction) likeButtonAction: (id)sender{
    
}
//------------------------------------------------------
#pragma mark - RDImageSliderDelegate Methods
//------------------------------------------------------
- (void)eTBImageSlider:(RDImageSlider *)eTBImageSlider didSelectIndex:(NSInteger)index{
    SliderImageInfo *sliderImageInfo=(SliderImageInfo*)self.sliderImages[index];
    NSLog(@"PAge url:%@",sliderImageInfo.page_url);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sliderImageInfo.page_url]];
}

//------------------------------------------------------
#pragma mark - UITextView Delegate Methods
//------------------------------------------------------
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:placeHolderMessage]) textView.text = @"";
    
   // textView.textColor = [UIColor blackColor];
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""])textView.text = placeHolderMessage;
   // textView.textColor = [UIColor lightGrayColor];
    [textView resignFirstResponder];
}

- (void)textViewDidChange:(UITextView *)textView{
   //if ([textView.text isEqualToString:@""])textView.text = placeHolderMessage;
}

//------------------------------------------------------
#pragma  mark - API Methods
//------------------------------------------------------

-(void) TotalUserOnlineApi
{
    [SVProgressHUD showWithStatus:@"Please wait..." maskType:SVProgressHUDMaskTypeGradient];
    
    RDRequestManager *manager = [RDRequestManager manager];
    [manager POST:@"get_total_online_user.php" parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              [SVProgressHUD dismiss];

              NSDictionary *jsonDict = (NSDictionary *) responseObject;
              if([[jsonDict objectForKey:@"response"] isEqualToString:@"success"])
              {
                  self.nowOnlineConunterLabel.text = [NSString stringWithFormat:@"Now Online\n%@",[jsonDict objectForKey:@"count"]];
              }
              
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              [SVProgressHUD dismiss];
              
              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
              [alert show];
          }
     ];
}

-(void)goOnline
{
    NSString *userid = [[NSUserDefaults standardUserDefaults] objectForKey:@"userIDKey"];

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:userid forKey:@"uuid"];
    RDRequestManager *manager = [RDRequestManager manager];
    [manager GET:@"user_online.php" parameters:params
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
           
              NSDictionary *jsonDict = (NSDictionary *) responseObject;
              if([[jsonDict objectForKey:@"response"] isEqualToString:@"success"])
              {
                 // self.lblNowOnline.text = [NSString stringWithFormat:@"%@",[jsonDict objectForKey:@"count"]];
              }
              
          }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:error.description delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
              [alert show];
          }
     ];
}
-(void)getSliderImages
{
    RDRequestManager *manager = [RDRequestManager manager];
    [manager GET:@"get_adurl.php" parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             NSDictionary *jsonDict = (NSDictionary *) responseObject;
             
             self.sliderImages=[SliderImageInfo arrayOfModelsFromDictionaries:jsonDict[@"adurl"]];
             
             [self.imageSlider setImages:[self.sliderImages valueForKey:@"image_url"]];
             
         }
     
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             
             
             //              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:error.description delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
             //              [alert show];
         }
     ];
}
@end