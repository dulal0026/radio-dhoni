//
//  ArchiveVC.h
//  Radio Dhoni
//
//  Created by MAC on 10/12/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArchiveVC : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *programCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *newsCollectionView;
- (IBAction)showMenu:(id)sender;
@end
