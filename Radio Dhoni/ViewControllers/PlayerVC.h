//
//  PlayerVC.h
//  Radio Dhoni
//
//  Created by MAC on 10/8/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "SliderImageInfo.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>

static NSString * const kClientId = @"YOUR_CLIENT_ID";

@interface PlayerVC : UIViewController<RDImageSliderDelegate,UITextViewDelegate>

@property (strong,nonatomic) AVPlayer *player;
@property (nonatomic, assign) BOOL isPlay;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *nowOnlineConunterLabel;
@property (strong, nonatomic) NSArray *sliderImages;
@property (weak, nonatomic) IBOutlet UIView *imageSliderContainer;
@property (weak, nonatomic) IBOutlet UITextView *messageTxtView;

@property (strong, nonatomic) RDImageSlider *imageSlider;
- (IBAction)playPauseButtonAction:(id)sender;
- (IBAction)showMenu:(id)sender;
- (IBAction)powerButtonAction:(id)sender;
- (IBAction)facebookShareButtonAction:(id)sender;
-(IBAction)twitterButtonAction:(id)sender;
- (IBAction) googleShareButtonAction: (id)sender;
- (IBAction) sendButtonAction: (id)sender;
- (IBAction) likeButtonAction: (id)sender;
@end