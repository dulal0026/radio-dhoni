//
//  ArchiveVC.m
//  Radio Dhoni
//
//  Created by MAC on 10/12/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import "ArchiveVC.h"
#import "UIViewController+MMDrawerController.h"

@implementation ArchiveVC

-(void)viewDidLoad{
    [super viewDidLoad];
    [self.programCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ImageCollectionViewCell class]) bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:NSStringFromClass([ImageCollectionViewCell class])];
    [self.newsCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ImageCollectionViewCell class]) bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:NSStringFromClass([ImageCollectionViewCell class])];
}
- (IBAction)showMenu:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:NULL];
}
//-----------------------------------------------------
#pragma mark - UICollectionViewDatasource
//-----------------------------------------------------

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ImageCollectionViewCell *cell;
    if (collectionView==self.programCollectionView) {
        cell = (ImageCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ImageCollectionViewCell class]) forIndexPath:indexPath];
         [cell.imageView sd_setImageWithURL:[NSURL URLWithString:@"http://104.236.22.155/uploads/1f78ea79f2991a07637661e1e782bd7e.png"]];
    }
    else if(collectionView==self.newsCollectionView){
        cell = (ImageCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ImageCollectionViewCell class]) forIndexPath:indexPath];
         [cell.imageView sd_setImageWithURL:[NSURL URLWithString:@"http://104.236.22.155/uploads/1f78ea79f2991a07637661e1e782bd7e.png"]];
    }
    
    
   // PictureModel *picture = [self MCATData].picture[indexPath.row];
    
   // cell.imageView.previewDataSource = self;

    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView==self.programCollectionView) return CGSizeMake(self.programCollectionView.frame.size.width/2-10, self.programCollectionView.frame.size.width/2-10);
    else return CGSizeMake(self.newsCollectionView.frame.size.width/2-10, self.newsCollectionView.frame.size.width/2-10);
}
@end
