//
//  MainVC.m
//  Radio Dhoni
//
//  Created by MAC on 10/8/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import "MainVC.h"

@interface MainVC ()

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.centerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Home_NavCont_ID"];
    self.leftDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Side_VC_ID"];
    
    self.openDrawerGestureModeMask = MMOpenDrawerGestureModePanningNavigationBar;
    self.closeDrawerGestureModeMask = MMCloseDrawerGestureModeBezelPanningCenterView |
    MMCloseDrawerGestureModeTapCenterView |
    MMCloseDrawerGestureModePanningNavigationBar |
    MMCloseDrawerGestureModeTapNavigationBar;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
