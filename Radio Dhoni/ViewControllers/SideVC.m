//
//  SideVC.m
//  Radio Dhoni
//
//  Created by MAC on 10/8/15.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import "SideVC.h"

NSString * const AAPMenuTitleKey = @"title";
NSString * const AAPMenuIconKey = @"icon";
@interface SideVC ()
{
    
}
@property (copy, nonatomic) NSArray *menus;
@end

@implementation SideVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.menus = @[
                   @{AAPMenuTitleKey :NSLocalizedString(@"Home", @"Home") , AAPMenuIconKey : @"Logo"},
                   @{AAPMenuTitleKey :NSLocalizedString(@"Archive", @"Archive") , AAPMenuIconKey : @"Logo"},
                   @{AAPMenuTitleKey :NSLocalizedString(@"Live Youtube", @"Live Youtube") , AAPMenuIconKey : @"Logo"}
                   ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
     return [self.menus count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AAPMenuCell *cell = (AAPMenuCell *)[tableView dequeueReusableCellWithIdentifier:@"CELL_ID" forIndexPath:indexPath];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:41.0/256.0 green:41.0/256.0 blue:41.0/256.0 alpha:1];
    //bgColorView.layer.cornerRadius = 7;
    bgColorView.layer.masksToBounds = YES;
    [cell setSelectedBackgroundView:bgColorView];
    
    // Configure the cell...
    NSDictionary *info = self.menus[indexPath.row];
    cell.backView.layer.cornerRadius=3.0f;
    cell.titleLabel.text = info[AAPMenuTitleKey];
    cell.iconView.image = [UIImage imageNamed:info[AAPMenuIconKey]];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row==0){
        UINavigationController *navCont=(UINavigationController*)self.mm_drawerController.centerViewController;
        [navCont popToRootViewControllerAnimated:NO];

        [self.mm_drawerController closeDrawerAnimated:YES completion:NULL];
    }
    else if (indexPath.row==1){
       [self performNavigation:@"archiveSegue"];
    }
    else if (indexPath.row==2){
        [self performNavigation:@"youtubeSegue"];
    }
}

-(void)performNavigation:(NSString*)segueIdentifier{
    UINavigationController *navCont=(UINavigationController*)self.mm_drawerController.centerViewController;
    [navCont popToRootViewControllerAnimated:NO];
    [navCont.viewControllers[0] performSegueWithIdentifier:segueIdentifier sender:self];
    [self.mm_drawerController closeDrawerAnimated:YES completion:NULL];
}

@end
@implementation AAPMenuCell

@end
