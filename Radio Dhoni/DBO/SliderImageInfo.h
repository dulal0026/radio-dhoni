//
//  SliderImageInfo.h
//  ETB
//
//  Created by AAPBD Mac mini on 28/01/2015.
//  Copyright (c) 2015 AAPBD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
@interface SliderImageInfo : JSONModel
@property (nonatomic, retain) NSString * image_url;
@property (nonatomic, retain) NSString * page_url;

@end
